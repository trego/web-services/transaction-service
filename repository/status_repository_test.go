package repository

import (
	"fmt"
	"testing"

	"gitlab.com/trego/web-services/transaction-service/db"
	"gitlab.com/trego/web-services/transaction-service/model"
)

func TestStatusRepositoryGet(t *testing.T) {
	db.Reset()
	(&model.Status{}).Seed(db.Get())

	statusRepo := &StatusRepository{
		db: _testDB,
	}
	statusList := statusRepo.Get()

	if len(statusList) != 6 {
		t.Errorf("StatusRepository.Get() doesn't return expected len 6")
	}
}

func TestStatusRepositoryCreate(t *testing.T) {
	db.Reset()
	statusRepo := &StatusRepository{
		db: _testDB,
	}

	newStatus := model.Status{
		Name: _testFaker.Person().Name(),
	}

	err := statusRepo.Create(&newStatus)
	if err != nil {
		t.Errorf("StatusRepository.Create() failed with error")
		t.Errorf(err.Error())
	}

	if newStatus.ID == 0 {
		t.Errorf("StatusRepository.Create() failed to save new data")
	}
}

func TestStatusRepositoryFind(t *testing.T) {
	db.Reset()
	(&model.Status{}).Seed(db.Get())

	statusRepo := &StatusRepository{
		db: _testDB,
	}

	expectedID := 1
	actual := statusRepo.Find(uint(expectedID))

	if actual.ID != uint(expectedID) {
		t.Errorf(fmt.Sprintf("StatusRepository.Find() failed to find expected ID %d", expectedID))
	}
}

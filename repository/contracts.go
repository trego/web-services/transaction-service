package repository

import (
	"gitlab.com/trego/web-services/transaction-service/model"
)

// StatusRepositoryContract represents interface
// of Status repository
type StatusRepositoryContract interface {
	Get() []model.Status
	Create(data *model.Status) error
	Find(id uint) model.Status
}

package repository

import "github.com/jinzhu/gorm"

// NewStatusRepository returns new StatusRepository instance
func NewStatusRepository(db *gorm.DB) *StatusRepository {
	return &StatusRepository{
		db: db,
	}
}

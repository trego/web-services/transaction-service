package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/trego/web-services/transaction-service/model"
)

// StatusRepository represents repository for Status
type StatusRepository struct {
	db *gorm.DB
}

// Get returns all Statuses
func (r *StatusRepository) Get() []model.Status {
	statuses := make([]model.Status, 0)

	r.db.Find(&statuses)

	return statuses
}

// Create stores new Status
func (r *StatusRepository) Create(data *model.Status) error {
	return r.db.Create(data).Error
}

// Find returns Status by ID.
func (r *StatusRepository) Find(id uint) model.Status {
	data := model.Status{}

	r.db.Where("id = ?", id).First(&data)

	return data
}

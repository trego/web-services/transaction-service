FROM golang AS build

WORKDIR /go/src/gitlab.com/trego/web-services/transaction-service

ADD . .

RUN go get -v ./...
RUN CGO_ENABLED=0 go build

FROM alpine

WORKDIR /usr/local/bin

COPY --from=build /go/src/gitlab.com/trego/web-services/transaction-service/transaction-service .
RUN chmod +x ./transaction-service

CMD ["transaction-service", "serve"]

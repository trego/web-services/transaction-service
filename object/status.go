package object

import (
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/trego/web-services/transaction-service/model"
)

// StatusResponse represents Status for response
type StatusResponse struct {
	ID        uint   `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}

// Render preprocesses the struct before rendered
func (res *StatusResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// CreateStatusResponse generates StatusResponse from Status
func CreateStatusResponse(data model.Status) render.Renderer {
	return &StatusResponse{
		ID:        data.ID,
		Name:      data.Name,
		CreatedAt: data.CreatedAt.Format("2006-01-02 15:04:05"),
		UpdatedAt: data.UpdatedAt.Format("2006-01-02 15:04:05"),
	}
}

// CreateStatusListResponse generates list of StatusResponse
func CreateStatusListResponse(data []model.Status) []render.Renderer {
	payload := make([]render.Renderer, 0)

	for _, item := range data {
		payload = append(payload, CreateStatusResponse(item))
	}

	return payload
}

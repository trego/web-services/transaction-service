package mock

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/trego/web-services/transaction-service/model"
)

// StatusRepository is mock of StatusRepository
type StatusRepository struct {
	mock.Mock
}

// Get mocks the Get
func (m *StatusRepository) Get() []model.Status {
	args := m.Called()

	return args.Get(0).([]model.Status)
}

// Create mocks the Create
func (m *StatusRepository) Create(data *model.Status) error {
	args := m.Called(data)

	return args.Error(0)
}

// Find mocks the Find
func (m *StatusRepository) Find(id uint) model.Status {
	args := m.Called(id)

	return args.Get(0).(model.Status)
}

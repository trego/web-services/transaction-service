package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // PostgreSQL driver
	"gitlab.com/trego/web-services/transaction-service/model"
)

var instance *gorm.DB

func initDB() (*gorm.DB, error) {
	connString := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_DATABASE"),
		os.Getenv("DB_PASSWORD"),
	)

	return gorm.Open("postgres", connString)
}

// Get returns DB connection singleton.
func Get() *gorm.DB {
	if instance == nil {
		conn, err := initDB()
		if err != nil {
			panic(err.Error())
		}

		instance = conn
	}
	return instance
}

// Migrate creates tables for available models.
func Migrate() {
	Get().AutoMigrate(
		model.Status{},
	)
}

// Drop removes all tables. What else?
func Drop() {
	Get().DropTableIfExists(
		model.Status{},
	)
}

// Reset runs Drop and Migrate.
func Reset() {
	Drop()
	Migrate()
}

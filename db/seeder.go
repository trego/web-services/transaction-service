package db

import "gitlab.com/trego/web-services/transaction-service/model"

// Seed runs the seeder.
func Seed() {
	seeder := []model.Seeder{
		&model.Status{},
	}

	db := Get()
	for _, item := range seeder {
		item.Seed(db)
	}
}

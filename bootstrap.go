package main

import (
	"gitlab.com/trego/web-services/transaction-service/db"
	"gitlab.com/trego/web-services/transaction-service/repository"
	"gitlab.com/trego/web-services/transaction-service/service"
)

var statusRepository repository.StatusRepositoryContract

var statusService service.StatusServiceContract

func initRepositories() {
	// Init repositories here
	dbConn := db.Get()

	statusRepository = repository.NewStatusRepository(dbConn)
}

func initServices() {
	// Init services here
	statusService = service.NewStatusService(statusRepository)
}

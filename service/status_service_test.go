package service

import (
	"reflect"
	"testing"

	localMock "gitlab.com/trego/web-services/transaction-service/mock"
	"gitlab.com/trego/web-services/transaction-service/model"
)

func TestStatusServiceGet(t *testing.T) {
	_mockRepo := &localMock.StatusRepository{}

	expected := []model.Status{
		model.Status{
			ID:   1,
			Name: _testFaker.Person().Name(),
		},
		model.Status{
			ID:   2,
			Name: _testFaker.Person().Name(),
		},
		model.Status{
			ID:   3,
			Name: _testFaker.Person().Name(),
		},
	}

	_mockRepo.On("Get").Return(expected)

	statusService := &StatusService{
		repo: _mockRepo,
	}

	actual := statusService.Get()

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("StatusService.Get() failed to return expected data")
	}
}

func TestStatusServiceCreate(t *testing.T) {
	_mockRepo := &localMock.StatusRepository{}
	_mockName := _testFaker.Person().Name()
	_mockPayload := model.Status{
		Name: _mockName,
	}

	expected := model.Status{
		ID:   0,
		Name: _mockName,
	}

	_mockRepo.On("Create", &_mockPayload).Return(nil)

	statusService := &StatusService{
		repo: _mockRepo,
	}

	actual, err := statusService.Create(_mockName)

	if err != nil {
		t.Errorf("StatusService.Create() failed with error")
		t.Errorf(err.Error())
	}

	if actual != expected {
		t.Errorf("StatusService.Create() failed to save data")
	}
}

func TestStatusServiceFind(t *testing.T) {
	_mockRepo := &localMock.StatusRepository{}

	expectedID := uint(1)
	expected := model.Status{
		ID:   expectedID,
		Name: _testFaker.Person().Name(),
	}

	_mockRepo.On("Find", expectedID).Return(expected, nil)

	statusService := &StatusService{
		repo: _mockRepo,
	}

	actual, err := statusService.Find(expectedID)

	if err != nil {
		t.Errorf("StatusService.Find() failed with error")
		t.Errorf(err.Error())
	}

	if actual.ID != expectedID {
		t.Errorf("StatusService.Find() failed to return expected data")
	}
}

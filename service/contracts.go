package service

import "gitlab.com/trego/web-services/transaction-service/model"

// StatusServiceContract represents interface
// for StatusService
type StatusServiceContract interface {
	Get() []model.Status
	Create(name string) (model.Status, error)
	Find(id uint) (model.Status, error)
}

package service

import (
	"fmt"

	"gitlab.com/trego/web-services/transaction-service/model"
	"gitlab.com/trego/web-services/transaction-service/repository"
)

// StatusService represents service for
// Status
type StatusService struct {
	repo repository.StatusRepositoryContract
}

// Get returns available Status
func (s *StatusService) Get() []model.Status {
	return s.repo.Get()
}

// Create creates new Status
func (s *StatusService) Create(name string) (model.Status, error) {
	newStatus := model.Status{
		Name: name,
	}

	if err := s.repo.Create(&newStatus); err != nil {
		return model.Status{}, err
	}
	return newStatus, nil
}

// Find returns Status by ID
func (s *StatusService) Find(id uint) (model.Status, error) {
	status := s.repo.Find(id)

	if status.ID == 0 {
		return model.Status{}, fmt.Errorf("Status with ID %d not found", id)
	}
	return status, nil
}

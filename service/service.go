package service

import (
	"gitlab.com/trego/web-services/transaction-service/repository"
)

// NewStatusService returns new StatusService instance
func NewStatusService(repo repository.StatusRepositoryContract) *StatusService {
	return &StatusService{
		repo: repo,
	}
}

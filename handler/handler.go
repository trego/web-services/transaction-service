package handler

import (
	"gitlab.com/trego/web-services/transaction-service/service"
)

// NewStatusHandler returns new StatusHandler instance
func NewStatusHandler(srv service.StatusServiceContract) *StatusHandler {
	return &StatusHandler{
		srv: srv,
	}
}

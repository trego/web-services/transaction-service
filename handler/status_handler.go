package handler

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/trego/web-services/transaction-service/object"
	"gitlab.com/trego/web-services/transaction-service/service"
)

// StatusHandler represents set of routes
// for Status
type StatusHandler struct {
	srv service.StatusServiceContract
}

// GetRoutes returns routes of this handler
func (h *StatusHandler) GetRoutes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", h.Get)

	return r
}

// Get doc
func (h *StatusHandler) Get(w http.ResponseWriter, r *http.Request) {
	statusListResponse := object.CreateStatusListResponse(h.srv.Get())

	if err := render.RenderList(w, r, statusListResponse); err != nil {
		fmt.Println(err.Error())
		return
	}
}

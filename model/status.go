package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Status represents status of a transaction
type Status struct {
	ID        uint
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// Seed runs data seeding.
func (m *Status) Seed(db *gorm.DB) {
	data := []Status{
		Status{
			ID:   1,
			Name: "Pending",
		},
		Status{
			ID:   2,
			Name: "Paid",
		},
		Status{
			ID:   3,
			Name: "Shipping",
		},
		Status{
			ID:   4,
			Name: "Rejected",
		},
		Status{
			ID:   5,
			Name: "Completed",
		},
		Status{
			ID:   6,
			Name: "Refund",
		},
	}

	for _, item := range data {
		found := Status{}
		db.Where("id = ?", item.ID).First(&found)
		if found.ID != 0 {
			continue
		}

		item.ID = 0
		db.Create(&item)
	}
}

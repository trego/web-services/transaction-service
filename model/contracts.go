package model

import "github.com/jinzhu/gorm"

// Seeder contract
type Seeder interface {
	Seed(db *gorm.DB)
}
